=head1 lustitia-lib.pl

Functions for the Lustitia Module. Configure lustitia scripts from Webmin!

=cut

use WebminCore;
init_config();

=head2 read_data()

Creates a data structure, read $config{'lustitia_conf'} and fills up structure.

=cut
sub read_data
{
	my %rv = ( 
		eth => {
			out => "" ,
			out_ip => "",
			out_mask => "",
			in => "",
			in_ip => "",
			in_mask => ""
		},

		other => {
			test_ips => "",
			default_router_ports => "",
			default_router_networks => "",
			local_networks => ""
		},

		router => [
			{
				ip => "",
				ipmon => "",
				mask => "",
				rate => "",
				ratio1 => "",
				ratio2 => "",
				ratio3 => "",
				weight => "",
				enable => ""
			},
			{
				ip => "",
				ipmon => "",
				mask => "",
				rate => "",
				ratio1 => "",
				ratio2 => "",
				ratio3 => "",
				weight => "",
				enable => ""
			},
			{
				ip => "",
				ipmon => "",
				mask => "",
				rate => "",
				ratio1 => "",
				ratio2 => "",
				ratio3 => "",
				weight => "",
				enable => ""
			},
			{
				ip => "",
				ipmon => "",
				mask => "",
				rate => "",
				ratio1 => "",
				ratio2 => "",
				ratio3 => "",
				weight => "",
				enable => ""
			},
			{
				ip => "",
				ipmon => "",
				mask => "",
				rate => "",
				ratio1 => "",
				ratio2 => "",
				ratio3 => "",
				weight => "",
				enable => ""
			}
		]
	);
	open(CONFIG, $config{'lustitia_conf'});
	while(<CONFIG>) {
		s/\r|\n//g;
		s/#.*$//;
		s/ *= */=/;
		if ( m/^ETH_/ ) {
			if ( m/OUT=(.*)/ ) {
				$rv{'eth'}->{'out'} = $1;
			}
			elsif ( m/OUT_IP=(.*)/ ) {
				$rv{'eth'}->{'out_ip'} = $1;
			}
			elsif ( m/OUT_MASK=(.*)/ ) {
				$rv{'eth'}->{'out_mask'} = $1;
			}
			if ( m/IN=(.*)/ ) {
				$rv{'eth'}->{'in'} = $1;
			}
			elsif ( m/IN_IP=(.*)/ ) {
				$rv{'eth'}->{'in_ip'} = $1;
			}
			elsif ( m/IN_MASK=(.*)/ ) {
				$rv{'eth'}->{'in_mask'} = $1;
			}
		}
		elsif ( m/^ROUTER_/ ) {
			if ( m/([0-9])_IP=(.*)/ ) {
				$rv{'router'}[$1]{'ip'} = $2;
			}
			if ( m/([0-9])_IPMON=(.*)/ ) {
				$rv{'router'}[$1]{'ipmon'} = $2;
			}
			if ( m/([0-9])_MASK=(.*)/ ) {
				$rv{'router'}[$1]{'mask'} = $2;
			}
			elsif ( m/([0-9])_RATE=(.*)/ ) {
				$rv{'router'}[$1]{'rate'} = $2;
			}
			elsif ( m/([0-9])_RATIO1=(.*)/ ) {
				$rv{'router'}[$1]{'ratio1'} = $2;
			}
			elsif ( m/([0-9])_RATIO2=(.*)/ ) {
				$rv{'router'}[$1]{'ratio2'} = $2;
			}
			elsif ( m/([0-9])_RATIO3=(.*)/ ) {
				$rv{'router'}[$1]{'ratio3'} = $2;
			}
			elsif ( m/([0-9])_WEIGHT=(.*)/ ) {
				$rv{'router'}[$1]{'weight'} = $2;
			}
			elsif ( m/([0-9])_ENABLE=(.*)/ ) {
				$rv{'router'}[$1]{'enable'} = $2;
			}
		}

		elsif ( m/^TEST_IPS=\"(.*)\"/ ) {
			$rv{'other'}->{'test_ips'} = $1;
		}
		elsif ( m/^DEFAULT_ROUTER_PORTS=\"(.*)\"/ ) {
			$rv{'other'}->{'default_router_ports'} = $1;
		}
		elsif ( m/^DEFAULT_ROUTER_NETWORKS=\"(.*)\"/ ) {
			$rv{'other'}->{'default_router_networks'} = $1;
		}
		elsif ( m/^LOCAL_NETWORKS=\"(.*)\"/ ) {
			$rv{'other'}->{'local_networks'} = $1;
		}
	}
	close(CONFIG);
	return %rv;
}

=head2 modify_lustitia_conf(&name, &value)

Modifies the given variable value in the $config{'lustitia_conf'} configuration file.

=cut
sub modify_lustitia_conf
{
	my $ref = shift;
	open_tempfile(CONF, "$config{'lustitia_conf'}");
	my $tmp = $/;
	local $/ = undef;
	my $lines = <CONF>;
	close_tempfile(CONF);
	foreach (@$ref) {
		m/(.*)=.*/;
		$lines =~ s/$1.*?\n/$_\n/;
	}
	$/ = $tmp;
	open_tempfile(CONF, ">$config{'lustitia_conf'}");
	print_tempfile(CONF, $lines);
	close_tempfile(CONF);
}

=head2 check_percent()

Returns 0 if the parameter value is between 0 and 100, 1 otherwise.

=cut
sub check_percent
{
	my $param = shift;
	if ($param > 100 || $param < 0) {
		return 0;
	}
	return 1;
}

=head2 repeated_ip()

Returns 0 if no IP is repeated between enabled configuration variables, 1 otherwise.
Enabled variables are ETH_OUT_IP, ETH_IN_IP and each ROUTER_N_IP/ROUTER_N_IPMON where ROUTER_N_ENABLE variable is equal to 1 (substitute N with each 0 to 4 numbers).

=cut
sub repeated_ip
{
	my $text = shift;
	my @split_text = split(/\n/,$text);
	# Who's enabled
	my @enabled;
	foreach (@split_text)
	{
		if (/ROUTER_(.)_ENABLE=1/) {
			push(@enabled,$1);
		}
	}
	# Add enabled routers IPs
	my @lines;
	foreach my $n (@enabled)
	{
		foreach (@split_text) {
			if (/ROUTER_${n}_IP*/) {
				push(@lines,$_."\n");
			}
		}
	}
	# Add interfaces IPs
	foreach (@split_text)
	{
		if (/ETH_OUT_IP/) {
			push(@lines,$_."\n");
		}
		elsif (/ETH_IN_IP/) {
			push(@lines,$_."\n");
		}	
	}
	# Let's count!
	my $count;
	my $ip = join('',@lines);
	foreach (@lines) 
	{
		if (/.*=(\d+\.\d+\.\d+\.\d+)/) {
			$count = 0;
			$count = () = $ip =~ /[^\d]\Q$1\E[^\d]/g;
			if ($count > 1) { return 1; }
		}
	}
	return 0;
}

=head2 apply_configuration()

Signal the desired process to re-read it's configuration files.

=cut
sub apply_configuration
{
	open_tempfile(CONF, "$config{'lustitia_conf'}");
	my $lines = <CONF>;
	close_tempfile(CONF);
	if (repeated_ip($lines)) {
		error($text{'err_dup_ip'});
	}
	else {
		execute_command("/etc/init.d/lustitia_rc restart", undef, undef, undef);
	}
}


1;
