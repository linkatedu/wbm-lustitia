#!/usr/bin/perl

require 'lustitia-lib.pl';

ui_print_header(undef, $module_info{'desc'}, "", undef, 1, 1);

# Parsing $config{lustitia_conf} file
my %data = read_data();

print text('explanation');

my @table = ( );

# Fill @table with table text.
for ($i = 0; $i < 5; $i++) {
	push(@table, ["<a href='edit.cgi?page=router".urlize($i)."'>".html_escape($text{'router'}." ".$i)."</a>", html_escape($data{'router'}[$i]{'enable'}?$text{'router_abs_enable'}." ".$data{'router'}[$i]{'ip'}."/".$data{'router'}[$i]{'mask'}:$text{'router_abs_disable'})]);
}

push(@table, ["<a href='edit.cgi?page=eth'>".$text{'interfaces'}."</a>", $text{'interfaces_abs'}." ".$data{'eth'}{'out'}]);

push(@table, ["<a href='edit.cgi?page=other'>".$text{'other'}."</a>", ""]);


# Print HTML form. Table data is stored in @table array.
print ui_form_columns_table(
#	"apply.cgi",
#	[ [ undef, $text{'apply'} ] ],
	undef,
	undef,
	0,
	undef,
	undef,
	[ $text{'element'}, $text{'abstract'} ],
	100,
	\@table,
	undef,
	0,
	undef,
	$text{'empty'}
);

print &ui_hr();
print "<table width=100%><tr>\n";
print "<form action=apply.cgi>\n";
print "<td><input type=submit value='$text{'apply'}'></td>\n";
print "<td>$text{'applymsg'}</td>\n";
print "</form></tr></table><br>\n";

ui_print_footer('/', $text{'index'});
