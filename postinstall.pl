do 'lustitia-lib.pl';

=head2 module_install(mode)

After module install a backup of the $config{'lustitia_conf'} file is done

=cut
sub module_install 
{
	if (!-r $config{'lustitia_conf'}.".bk") {
		copy_source_dest($config{'lustitia_conf'}, $config{'lustitia_conf'}.".bk");
	}
}
