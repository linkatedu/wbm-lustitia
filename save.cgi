#!/usr/bin/perl
# Save changes in $config{foorbar_tags} file

require 'lustitia-lib.pl';

ReadParse();
error_setup($text{'save_err'});
lock_file($config{'lustitia_conf'});
my @data = ( );
if ($in{'page'} =~ m/router([0-9])/) {
	if (! check_ipaddress($in{"ROUTER_$1_IP"})) {
		error($text{'err_ip'});
	}
	if (! check_ipaddress($in{"ROUTER_$1_IPMON"})) {
		error($text{'err_ip'});
	}
	if (! check_ipaddress($in{"ROUTER_$1_MASK"})) {
		error($text{'err_mask'});
	}
	if (! check_percent($in{"ROUTER_$1_RATIO1"})) {
		error($text{'err_percent'});
	}
	if (! check_percent($in{"ROUTER_$1_RATIO2"})) {
		error($text{'err_percent'});
	}
	if (! check_percent($in{"ROUTER_$1_RATIO3"})) {
		error($text{'err_percent'});
	}
	if (! check_percent(($in{"ROUTER_$1_RATIO1"}+$in{"ROUTER_$1_RATIO2"}+$in{"ROUTER_$1_RATIO3"}))) {
		error($text{'err_total_percent'});
	}
	push(@data, "ROUTER_$1_IP=".$in{"ROUTER_$1_IP"});	
	push(@data, "ROUTER_$1_IPMON=".$in{"ROUTER_$1_IPMON"});	
	push(@data, "ROUTER_$1_MASK=".$in{"ROUTER_$1_MASK"});	
	push(@data, "ROUTER_$1_RATE=".$in{"ROUTER_$1_RATE"});	
	push(@data, "ROUTER_$1_RATIO1=".$in{"ROUTER_$1_RATIO1"});	
	push(@data, "ROUTER_$1_RATIO2=".$in{"ROUTER_$1_RATIO2"});	
	push(@data, "ROUTER_$1_RATIO3=".$in{"ROUTER_$1_RATIO3"});	
	push(@data, "ROUTER_$1_WEIGHT=".$in{"ROUTER_$1_WEIGHT"});	
	push(@data, "ROUTER_$1_ENABLE=".($in{"ROUTER_$1_ENABLE"}?1:0));	
}
elsif ($in{'page'} == "other") {
	push(@data, "TEST_IPS=\"".$in{"TEST_IPS"}."\"");
	push(@data, "DEFAULT_ROUTER_PORTS=\"".$in{"DEFAULT_ROUTER_PORTS"}."\"");
	push(@data, "DEFAULT_ROUTER_NETWORKS=\"".$in{"DEFAULT_ROUTER_NETWORKS"}."\"");
	push(@data, "LOCAL_NETWORKS=\"".$in{"LOCAL_NETWORKS"}."\"");
}
else {
	if (! check_ipaddress($in{"ETH_OUT_IP"})) {
		error($text{'err_ip'});
	}
	if (! check_ipaddress($in{"ETH_OUT_MASK"})) {
		error($text{'err_mask'});
	}
	if (! check_ipaddress($in{"ETH_IN_IP"})) {
		error($text{'err_ip'});
	}
	if (! check_ipaddress($in{"ETH_IN_MASK"})) {
		error($text{'err_mask'});
	}
	push(@data, "ETH_OUT=".$in{"ETH_OUT"});	
	push(@data, "ETH_OUT_IP=".$in{"ETH_OUT_IP"});	
	push(@data, "ETH_OUT_MASK=".$in{"ETH_OUT_MASK"});	
	push(@data, "ETH_IN=".$in{"ETH_IN"});	
	push(@data, "ETH_IN_IP=".$in{"ETH_IN_IP"});	
	push(@data, "ETH_IN_MASK=".$in{"ETH_IN_MASK"});	
}
modify_lustitia_conf(\@data);
webmin_log("Modification","of page",$in{'page'});
unlock_file($config{'lustitia_conf'});
&redirect('');
