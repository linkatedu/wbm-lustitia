#!/usr/bin/perl
# Edit form.

require 'lustitia-lib.pl';

ReadParse();

my %data = read_data();

my @table = ( ); 

# Show page header and get the site being edited
ui_print_header(undef, $module_info{'desc'}, "", undef, 1, 1);

# Distinguish between ethernet and router configuration.
if ($in{'page'} eq "eth" ) {
	# Ethernet configuration.
	print text('explanation_eth');
	push(@table,[	"<b>".$text{'eth_out'}."</b>",
			ui_textbox("ETH_OUT", $data{'eth'}{'out'}, 25)
			]);
	push(@table,[	"<b>".$text{'eth_in'}."</b>",
			ui_textbox("ETH_IN", $data{'eth'}{'in'}, 25)
			]);
} elsif ($in{'page'} eq "other" ) {
	print text('other');
	push(@table,[	"<b>".$text{'test_ips'}."</b>",
			ui_textbox("TEST_IPS", $data{'other'}{'test_ips'}, 25)
			]);
	push(@table,[	"<b>".$text{'default_router_ports'}."</b>",
			ui_textbox("DEFAULT_ROUTER_PORTS", $data{'other'}{'default_router_ports'}, 25)
			]);
	push(@table,[	"<b>".$text{'default_router_networks'}."</b>",
			ui_textbox("DEFAULT_ROUTER_NETWORKS", $data{'other'}{'default_router_networks'}, 25)
			]);
	push(@table,[	"<b>".$text{'local_networks'}."</b>",
			ui_textbox("LOCAL_NETWORKS", $data{'other'}{'local_networks'}, 25)
			]);
} else {
	# Router configuration.
	$in{'page'} =~ m/router([0-9])/;
	print text('explanation_router',"$1");
	push(@table,[	"<b>".$text{'router_ip'}."</b>",
			ui_textbox("ROUTER_$1_IP", $data{'router'}[$1]{'ip'}, 25)
			]);
	push(@table,[	"<b>".$text{'router_ipmon'}."</b>",
			ui_textbox("ROUTER_$1_IPMON", $data{'router'}[$1]{'ipmon'}, 25)
			]);
	push(@table,[	"<b>".$text{'router_mask'}."</b>",
			ui_textbox("ROUTER_$1_MASK", $data{'router'}[$1]{'mask'}, 25)
			]);
	push(@table,[	"<b>".$text{'router_rate'}."</b>",
			ui_textbox("ROUTER_$1_RATE", $data{'router'}[$1]{'rate'}, 25)
			]);
	push(@table,[	"<b>".$text{'router_ratio1'}."</b>",
			ui_textbox("ROUTER_$1_RATIO1", $data{'router'}[$1]{'ratio1'}, 25)
			]);
	push(@table,[	"<b>".$text{'router_ratio2'}."</b>",
			ui_textbox("ROUTER_$1_RATIO2", $data{'router'}[$1]{'ratio2'}, 25)
			]);
	push(@table,[	"<b>".$text{'router_ratio3'}."</b>",
			ui_textbox("ROUTER_$1_RATIO3", $data{'router'}[$1]{'ratio3'}, 25)
			]);
	push(@table,[	"<b>".$text{'router_weight'}."</b>",
			ui_textbox("ROUTER_$1_WEIGHT", $data{'router'}[$1]{'weight'}, 25)
			]);
	push(@table,[	"<b>".$text{'router_enable'}."</b>",
			ui_checkbox("ROUTER_$1_ENABLE", 1, "",$data{'router'}[$1]{'enable'})
			]);

}

# Print HTML form. Table data is stored in @table array. 
print ui_form_columns_table(
	"save.cgi?page=$in{'page'}",
	[ [ undef, $text{'save'} ], ui_reset($text{'reset'}) ],
	0,
	undef,
	undef,
	[ $text{'name'}, $text{'value'} ],
	100,
	\@table,
	undef,
	0,
	undef,
	undef
	);

ui_print_footer('', $text{'index_return'});

